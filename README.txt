WIP

Description:
============

The itsme module provides a service in Drupal 8 for
- logging in users that are authenticated against the remote itsme database
- storing the authentication details.

Installation:
=============

Installation of this module is just like any other Drupal module.

1) Download the module
2) Uncompress it
3) Move it to the appropriate modules directory (/modules/custom)
4) Go to the Drupal module administration page for your site
5) Enable the module